package io.intcom.util;

import io.intcom.domain.Customer;

/**
 * @author Dinesh Thangaraj
 *
 *         Created on 06-Mar-2018
 */
public class DistanceCalculatorUtil {

	private static final Integer EARTH_RADIUS = 6371;

	private Double calculateDistance(Double customerLat, Double customerLong, Double intcomLat, Double intcomLong) {
		Double dLat = Math.toRadians((intcomLat - customerLat));
		Double dLong = Math.toRadians((intcomLong - customerLong));
		customerLat = Math.toRadians(customerLat);
		intcomLat = Math.toRadians(intcomLat);
		Double a = haversin(dLat) + Math.cos(customerLat) * Math.cos(intcomLat) * haversin(dLong);

		return 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)) * EARTH_RADIUS;
	}

	private Double haversin(Double val) {
		return Math.pow(Math.sin(val / 2), 2);
	}

	/**
	 * @param customer
	 * @param friendlyRadius
	 * @param customerLat
	 * @param customerLong
	 * @param intcomLat
	 * @param intcomLong
	 * @return If the customer location is within the friendly radius
	 */
	public boolean isWithinRadius(Customer customer, final Double friendlyRadius, final Double customerLat,
			final Double customerLong, final Double intcomLat, final Double intcomLong) {
		Double distance = calculateDistance(customerLat, customerLong, intcomLat, intcomLong);
		customer.setDistance(distance);
		return distance < friendlyRadius;
	}
}
