package io.intcom.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.intcom.domain.Customer;

/**
 * @author Dinesh Thangaraj
 *
 *         Created on 06-Mar-2018
 */
public class FileProcessorUtil {

	// private static final Logger LOGGER =
	// LoggerFactory.getLogger(FileProcessorUtil.class);

	public FileProcessorUtil() {
	}

	// Create similar Comparators as and when required for descending order and sort
	// based on other properties apart from name
	static Comparator<Customer> ascCustomerByName = (Customer c1, Customer c2) -> c1.getName().compareTo(c2.getName());

	/**
	 * @param jsonFile
	 *            path
	 * @param latLong
	 *            (contains friendly radius, latitude and longitude of IntCom )
	 * @return List of Customers in ascending order
	 */
	public List<Customer> readCustomerData(String jsonFile, Double[] latLong) {
		List<Customer> customers = new ArrayList<Customer>();
		DistanceCalculatorUtil distanceCalculatorUtil = new DistanceCalculatorUtil();
		try (BufferedReader br = new BufferedReader(new FileReader(jsonFile));) {
			for (String line = null; (line = br.readLine()) != null;) {
				try {
					com.google.gson.Gson gson = new com.google.gson.GsonBuilder().create();
					Customer customer = gson.fromJson(line, Customer.class);
					if (validateCustomer(customer)) {
						// Radius of 0 means 'invite all'
						// Radius of < 0 means 'invite none'
						if (latLong[0] >= 0d && (latLong[0] == 0d || distanceCalculatorUtil.isWithinRadius(customer,
								latLong[0], customer.getLatitude(), customer.getLongitude(), latLong[1], latLong[2]))) {
							// LOGGER.debug("Inviting customer: {}", customer);
							customers.add(customer);
						} else {
							// LOGGER.debug("Not inviting customer: {}", customer);
						}
					} else {
						// LOGGER.debug("Ill formed customer data. {} Skipping.", line);
					}
				} catch (Exception e) {
					// LOGGER.debug("Exception in reading the customer data: {} Skipping.", line);
				}
			}
		} catch (Exception e) {
			// LOGGER.debug("Exception in file processor {} Skipping.", e);
		}

		// This gives more flexibility than using priority queue
		Collections.sort(customers, ascCustomerByName);
		return customers;
	}

	private boolean validateCustomer(Customer customer) {
		// Note: 0 is a valid Lat and/or Long
		return customer.getLatitude() != null && customer.getLongitude() != null && customer.getName() != null
				&& customer.getName().trim().length() != 0 && customer.getLatitude() > -90
				&& customer.getLatitude() < 90 && customer.getLongitude() > -180 && customer.getLongitude() < 180;
	}
}
