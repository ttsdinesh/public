package io.intcom;

import io.intcom.util.FileProcessorUtil;

/**
 * @author Dinesh Thangaraj
 *
 *         Created on 06-Mar-2018
 */
public class FindNearestCustomersApplication {

	public static final String HELP_TEXT = "Missing/Wrong arguments. Usage java -jar target/find-nearest-customers-0.0.1-SNAPSHOT.jar customers.json friendlyRadius IntComLat IntComLong "
			+ "\n E.g: java -jar target/find-nearest-customers-0.0.1-SNAPSHOT.jar /tmp/customers.json 100 53.339428 -6.257664";

	public static void main(String[] args) {
		FindNearestCustomersApplication findNearestCustomers = new FindNearestCustomersApplication();
		findNearestCustomers.findNearestCustomers(args);
	}

	/**
	 * Print the list of customers with in the given radius in the following format
	 * 
	 * name : user_id
	 * 
	 * @param args
	 *            (jsonFilePath, radius, intComLat, intComLong)
	 */
	public void findNearestCustomers(String[] args) {
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		FindNearestCustomersApplication findNearestCustomers = new FindNearestCustomersApplication();
		Double[] latLong = new Double[3];
		if (findNearestCustomers.validateAndParseArgs(args, latLong)) {
			fileProcessorUtil.readCustomerData(args[0], latLong)
					.forEach(customer -> System.out.println(customer.getName() + " : " + customer.getUser_id()));
		} else {
			System.out.println(HELP_TEXT);
		}
	}

	private Boolean validateAndParseArgs(String[] args, Double[] latLong) {
		if (args == null || args.length != 4 || args[0] == null || args[0].trim().length() == 0) {
			return false;
		}
		try {
			latLong[0] = Double.valueOf(args[1]);
			latLong[1] = Double.valueOf(args[2]);
			latLong[2] = Double.valueOf(args[3]);
			if (latLong[1] > 90 || latLong[1] < -90 || latLong[2] > 180 || latLong[2] < -180)
				return false;

			return true;
		} catch (Exception e) {
			// LOGGER.debug("Exception in formatting the input args");
		}
		return false;
	}
}
