package io.intcom;

import java.io.File;

import io.intcom.domain.Customer;
import io.intcom.util.FileProcessorUtil;
import junit.framework.TestCase;

/**
 * @author Dinesh Thangaraj
 *
 *         Created on 06-Mar-2018
 */
public class FindNearestCustomersApplicationTests extends TestCase {

	// Case 1: Check if the world exists
	public void testIfTheWorldExists() {
		assertTrue(true);
		assertFalse(false);
	}

	// Case 2: Test if the json file exists
	public void testIfFileExists() {
		String jsonFilePath = System.getProperty("case2filePath");
		File jsonFile = new File(jsonFilePath);
		assertTrue(jsonFile.exists());
	}

	// Case 3: Friendly Radius is 100km
	public void testFirstInvitedCustomer100km() {
		String jsonFilePath = System.getProperty("case2filePath");
		Double friendlyRadius = Double.valueOf(System.getProperty("case3FriendlyRadius"));
		Double intComLat = Double.valueOf(System.getProperty("case3IntComLat"));
		Double intComLong = Double.valueOf(System.getProperty("case3IntComLong"));
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		Customer firstInvitedCustomer = new Customer(53.1489345, -6.8422408, 31, "Alan Behan");
		assertEquals(firstInvitedCustomer,
				fileProcessorUtil.readCustomerData(jsonFilePath, new Double[] { friendlyRadius, intComLat, intComLong })
						.stream().findFirst().get());
	}

	// Case 4: Friendly Radius is 40km
	public void testFirstInvitedCustomer40km() {
		String jsonFilePath = System.getProperty("case2filePath");
		Double friendlyRadius = Double.valueOf(System.getProperty("case4FriendlyRadius"));
		Double intComLat = Double.valueOf(System.getProperty("case3IntComLat"));
		Double intComLong = Double.valueOf(System.getProperty("case3IntComLong"));
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		Customer firstInvitedCustomer = new Customer(53.2451022, -6.238335, 4, "Ian Kehoe");
		assertEquals(firstInvitedCustomer,
				fileProcessorUtil.readCustomerData(jsonFilePath, new Double[] { friendlyRadius, intComLat, intComLong })
						.stream().findFirst().get());
	}

	// Case 5: Friendly Radius is 0km (Invite all customers)
	public void testFirstInvitedCustomer0km() {
		String jsonFilePath = System.getProperty("case2filePath");
		Double friendlyRadius = Double.valueOf(System.getProperty("case5FriendlyRadius"));
		Double intComLat = Double.valueOf(System.getProperty("case3IntComLat"));
		Double intComLong = Double.valueOf(System.getProperty("case3IntComLong"));
		int totalCustomers = Integer.valueOf(System.getProperty("case5TotalCustomers"));
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		assertEquals(totalCustomers, fileProcessorUtil
				.readCustomerData(jsonFilePath, new Double[] { friendlyRadius, intComLat, intComLong }).size());
	}

	// Case 6: Friendly Radius is -1km (Invite none)
	public void testFirstInvitedCustomerNeagativekm() {
		String jsonFilePath = System.getProperty("case2filePath");
		Double friendlyRadius = Double.valueOf(System.getProperty("case5FriendlyRadius"));
		Double intComLat = Double.valueOf(System.getProperty("case3IntComLat"));
		Double intComLong = Double.valueOf(System.getProperty("case3IntComLong"));
		int totalCustomers = Integer.valueOf(System.getProperty("case5TotalCustomers"));
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		assertEquals(totalCustomers, fileProcessorUtil
				.readCustomerData(jsonFilePath, new Double[] { friendlyRadius, intComLat, intComLong }).size());
	}

	// Case 7: Single incorrect customer data returns one less than usual for 100km
	public void testSingleIncorrectCustomerData() {
		String jsonFilePath = System.getProperty("case7filePath");
		Double friendlyRadius = Double.valueOf(System.getProperty("case3FriendlyRadius"));
		Double intComLat = Double.valueOf(System.getProperty("case3IntComLat"));
		Double intComLong = Double.valueOf(System.getProperty("case3IntComLong"));
		int totalCustomers = Integer.valueOf(System.getProperty("case7TotalCustomers"));
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		assertEquals(totalCustomers, fileProcessorUtil
				.readCustomerData(jsonFilePath, new Double[] { friendlyRadius, intComLat, intComLong }).size());
	}

	// Case 8: All correct data and returns all invited customers
	public void testAllcorrectCustomerData() {
		String jsonFilePath = System.getProperty("case2filePath");
		Double friendlyRadius = Double.valueOf(System.getProperty("case3FriendlyRadius"));
		Double intComLat = Double.valueOf(System.getProperty("case3IntComLat"));
		Double intComLong = Double.valueOf(System.getProperty("case3IntComLong"));
		int totalCustomers = Integer.valueOf(System.getProperty("case8TotalCustomers"));
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		assertEquals(totalCustomers, fileProcessorUtil
				.readCustomerData(jsonFilePath, new Double[] { friendlyRadius, intComLat, intComLong }).size());
	}

	// Case 9: Test latitude
	public void testCustomerLat() {
		String jsonFilePath = System.getProperty("case9filePath");
		Double friendlyRadius = Double.valueOf(System.getProperty("case3FriendlyRadius"));
		Double intComLat = Double.valueOf(System.getProperty("case3IntComLat"));
		Double intComLong = Double.valueOf(System.getProperty("case3IntComLong"));
		int totalCustomers = Integer.valueOf(System.getProperty("case9TotalCustomers"));
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		assertEquals(totalCustomers, fileProcessorUtil
				.readCustomerData(jsonFilePath, new Double[] { friendlyRadius, intComLat, intComLong }).size());
	}

	// Case 10: Test longitude
	public void testCustomerLong() {
		String jsonFilePath = System.getProperty("case10filePath");
		Double friendlyRadius = Double.valueOf(System.getProperty("case3FriendlyRadius"));
		Double intComLat = Double.valueOf(System.getProperty("case3IntComLat"));
		Double intComLong = Double.valueOf(System.getProperty("case3IntComLong"));
		int totalCustomers = Integer.valueOf(System.getProperty("case10TotalCustomers"));
		FileProcessorUtil fileProcessorUtil = new FileProcessorUtil();
		assertEquals(totalCustomers, fileProcessorUtil
				.readCustomerData(jsonFilePath, new Double[] { friendlyRadius, intComLat, intComLong }).size());
	}

}
