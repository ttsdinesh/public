# Find Nearest Customers

Find all the customers with in the given radius and display them in ascending order with respect to the name.

## Getting Started

### Prerequisites

- Maven
- JDK 1.8

### Compile and package
```
mvn clean package -DskipTests
```

### Execute  
```
java -jar target/find-nearest-customers-0.0.1-SNAPSHOT.jar customers.json 100 53.339428 -6.257664
```

### Execute test cases
 
```
mvn clean test
```
Note: Change the json file paths and other required parameters in the pom.xml to let the test cases complete

